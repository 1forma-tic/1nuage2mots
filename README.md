# 1nuage2mots
## [Online Demo](https://1forma-tic.frama.io/1nuage2mots/)

## Usage :
For examples, see [demo source code](demo/index.html.ejs).

Recommanded : [node.js](https://nodejs.org/).

In your project root, in cli :
```shell
npm install --save-dev 1nuage2mots
```
In your html when you want insert a word-cloud 1nuage2mots :
```html

<div class="_1nuage2mots">
    <div tag-value="5">Lorem</div>
    <div tag-value="1">Ipsum</div>
</div>

<link rel="stylesheet" href="../PATH_TO/node_modules/1nuage2mots/1nuage2mots.css"/>
<script src="../PATH_TO/node_modules/1nuage2mots/1nuage2mots.js"></script>
```
alternative :
- for css in stylus : `@import "1nuage2mots/*"`
- for js : `import _1nuage2mots from "/node_modules/1nuage2mots/1nuage2mots.mjs";`

## Options :
You can use xml attribute on `<div class="_1nuage2mots">` to customize 1nuage2mots behavior.

- **tag-size-min** : default 15 (px)
- **tag-size-max** : default 45 (px)
- **tag-size-unit** : default px
- **tag-size-pow** : default "auto". Advanced parameter to customize the pow function to compute the size curve between the biggest and the smallest tag.
- **tag-opacity-min** : default 1. The opacity for tag-value="0" (0 = 0%, 1 = 100%)
- **tag-opacity-max** : default 1. The opacity for the bigest tag-value (0 = 0%, 1 = 100%)
- **tag-opacity-pow** : default "auto". Power function (like tag-size-pow) for opacity curve between the most and the less visible tag
- **tag-chaos** : default 5. Advanced parameter to avoid similar consecutive tags. Increasing it may slow down (it's the max chaos render step)
- **tag-show** : default "1by1". How will the word cloud show : instant, 1by1, by value group
- **tag-show-start** : default "first time on screen". When the show animation start : (first/every) time (the page load/in screen or in view)
- **tag-show-interval** : default 1 (second). Time between 2 show step.
- **tag-flash** : default "no". If activated, each word will flash every x secondes periodically to get attention. Flash mode : none, glow, zoom/scale/bounce
- **tag-flash-interval** : default 3 (second). Time between 2 flash. You can use decimal (ex 0.5). Each flash last 2 second. Use CSS to overwrite duration.
