export async function sleep(ms){
    return new Promise((resolve,reject)=>setTimeout(resolve,ms));
}
export function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}
export function tryPreventingDefault(e){
    if(typeof e !== "undefined" && typeof e.preventDefault !== "undefined")e.preventDefault();
}
export default function init(){
    if(typeof window !== "undefined"){
        window.sleep = sleep;
        window.docReady = docReady;
        window.tryPreventingDefault = tryPreventingDefault;
    }
}
init();
