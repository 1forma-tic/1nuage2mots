
export default function ssgWordCloud(wordCloud){
    if(wordCloud.classList.contains('js')) return;
    wordCloud.classList.add('js');

    const cfg = { // default config and usable tags to overwrite it
        sizeMin: parseFloat(wordCloud.getAttribute('tag-size-min')) || 15, // in sizeUnit, default px
        sizeMax: parseFloat(wordCloud.getAttribute('tag-size-max')) || 45, // in sizeUnit, default px
        sizeUnit: wordCloud.getAttribute('tag-size-unit') || 'px',
        sizePow: parseFloat(wordCloud.getAttribute('tag-size-pow')) || "auto", // adv: size curve between the biggest and the smallest tag
        opacityMin: parseFloat(wordCloud.getAttribute('tag-opacity-min')) || 1, // the opacity for tag-value="0"
        opacityMax: parseFloat(wordCloud.getAttribute('tag-opacity-max')) || 1, // the opacity for the bigest tag-value
        opacityPow: parseFloat(wordCloud.getAttribute('tag-opacity-pow')) || "auto", // adv: opacity curve between the most and the less visible tag
        chaos: parseInt(wordCloud.getAttribute('tag-chaos')) || 5, // adv: max chaos pass to avoid similar consecutive tags
        show: wordCloud.getAttribute('tag-show') || "1by1", // show tag : instant, 1by1, by value group
        showStart: wordCloud.getAttribute('tag-show-start') || "first time on screen", // when start the shwo animtion (first/every) time (page load/in screen or view)
        showInterval: parseFloat(wordCloud.getAttribute('tag-show-interval')) || 1, // adv: time in second between 2 show step
        // each word will flash every x secondes periodicaly to get attention.
        flash: wordCloud.getAttribute('tag-flash') || "no", // flash mode : none, glow, zoom/scale/bounce
        flashInterval: parseFloat(wordCloud.getAttribute('tag-flash-interval')) || 3, // adv: time in second
    };
    // mechanism
    const context = {
        valueMax:0,
        valueLevel:{},
        valueLevelLength:0,
        colorVariety:{},
        colorVarietyLength:0
    }
    let tmpMax = 0;
    const initialCloud = [];
    for (let el of wordCloud.children){
        const val = getValue(el); // how big and visible will be this tag
        tmpMax = Math.max(tmpMax, val);
        if(!context.valueLevel[val]) context.valueLevel[val] = 1;
        else context.valueLevel[val]++;
        const color = getComputedStyle(el).getPropertyValue('color');
        if(!context.colorVariety[color]) context.colorVariety[color] = 1;
        else context.colorVariety[color]++;

        initialCloud.push({el, val, color});
    }
    if(0 === tmpMax) tmpMax = 1;
    context.valueMax = tmpMax;
    context.valueLevelLength = Object.keys(context.valueLevel).length;
    context.colorVarietyLength = Object.keys(context.colorVariety).length;
    if(cfg.sizePow === "auto") cfg.sizePow = wordCloud.children.length/context.valueLevelLength/context.valueLevel[context.valueMax];
    if(cfg.opacityPow === "auto") cfg.opacityPow = wordCloud.children.length/context.valueLevelLength/context.valueLevel[context.valueMax];

    const shuffledCloud = initialCloud.sort(() => Math.random() - 0.5);
    shuffledCloud.forEach(x => {
        const { val, el } = x;

        el.style.fontSize = `${0.01*Math.trunc( 100*Math.pow(val/context.valueMax,cfg.sizePow) * (cfg.sizeMax-cfg.sizeMin)) + cfg.sizeMin}${cfg.sizeUnit}`;
        el.style.opacity = `${0.01*Math.trunc(100*Math.pow(val/context.valueMax,cfg.opacityPow)*(cfg.opacityMax-cfg.opacityMin))+cfg.opacityMin}`;
        wordCloud.appendChild(el);
    });
    // extra chaos
    for(let chaos = 0;chaos<cfg.chaos;chaos++){
        let change = 0;
        for (let i in wordCloud.children){
            if(!wordCloud.children.hasOwnProperty(i)) continue;
            const cur = wordCloud.children[i];
            const prev = wordCloud.children[(Number(i)-1+wordCloud.children.length)%wordCloud.children.length];
            if(chaosTrigger(prev,cur,context)) {
                wordCloud.insertBefore(cur,wordCloud.children[Math.floor(Math.random()*wordCloud.children.length)].el);
                change++;
            }
        }
        if(!change) break;
    }

    wordCloud.pendingTimeout = [];
    let alreadyStarted = false;

    if(!!cfg.showStart.match(/load|begin|start|init/)) startShow(wordCloud,cfg,context);
    function showIfInView(){
        if(!alreadyStarted && inView(wordCloud,50).inside){
            startShow(wordCloud,cfg,context);
            alreadyStarted=true;
        }
        if(!!cfg.showStart.match('every') && !inView(wordCloud).inside) alreadyStarted=false;
    }
    if(!!cfg.showStart.match(/screen|view/)) {
        addEventListener('scroll',showIfInView);
        showIfInView();
    }
    // flash part
    if(!!cfg.flash.match(/ow|zoom|scale|bounce/)){
        let flashMode = "";
        if(!!cfg.flash.match(/ow/)) flashMode = "flashGlow";
        if(!!cfg.flash.match(/zoom|scale|bounce/)) flashMode = "flashScale";

        let flashIndex = 0;
        setInterval(()=>{
            const choosen = initialCloud[flashIndex%initialCloud.length].el;
            choosen.classList.add(flashMode);
            setTimeout(()=>choosen.classList.remove(flashMode),2_000);
            flashIndex++;
        },cfg.flashInterval*1000);
    }
}
function getColor(el){
    return getComputedStyle(el).getPropertyValue('color');
}
function getValue(el){
    return Number(el.getAttribute('tag-value')) || 0;
}
function chaosTrigger(prev,cur, context){
    if(context.colorVarietyLength>1 && getColor(prev) === getColor(cur)) return true;
    if(context.valueLevelLength>1 && Math.abs(getValue(prev) - getValue(cur)) <= Math.floor(context.valueLevelLength/5)) return true;
    return false;
}

async function startShow(wordCloud,cfg,context){
    while(wordCloud.pendingTimeout.length) clearTimeout(wordCloud.pendingTimeout.shift());
    wordCloud.classList.add('instantChange');
    await sleep(10);
    if(cfg.show){
        for (let el of wordCloud.children){
            el.classList.add('hide');
        }
        if(!!cfg.show.match(/(2|group|grp)/) ){
            for(let t = 0; t<=context.valueMax;t++){
                wordCloud.pendingTimeout.push(setTimeout(()=>{
                    wordCloud.querySelectorAll(`[tag-value="${t}"]`).forEach(el=>el.classList.remove('hide'))
                },(.1+context.valueMax-t)*cfg.showInterval*1000));
            }
        }
        if(!!cfg.show.match(/(1|one|un)/) ){
            let level = context.valueMax;
            for(let t = 0; t<=wordCloud.children.length;t++){
                if(!wordCloud.children.hasOwnProperty(t)) continue;
                wordCloud.pendingTimeout.push(setTimeout(()=>{
                    let candidates = wordCloud.querySelectorAll(`.hide[tag-value="${level}"]`);
                    if(!candidates.length){
                        level--;
                        candidates = wordCloud.querySelectorAll(`.hide[tag-value="${level}"]`);
                    }
                    candidates[Math.floor(Math.random()*candidates.length)].classList.remove('hide');
                },(.1+t)*cfg.showInterval*1000));
            }
        }
    }
    await sleep(10);
    wordCloud.classList.remove('instantChange');
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
// from https://github.com/Tokimon/vanillajs-browser-helpers and https://stackoverflow.com/questions/5353934/check-if-element-is-visible-on-screen
function inView(el, threshold = 0) {

    const rect = el.getBoundingClientRect();
    const vpWidth = window.innerWidth;
    const vpHeight = window.innerHeight;

    const above = rect.bottom - threshold <= 0;
    const below = rect.top - vpHeight + threshold >= 0;
    const left = rect.right - threshold <= 0;
    const right = rect.left - vpWidth + threshold >= 0;
    const inside = !above && !below && !left && !right;

    return { above, below, left, right, inside };
}

document.querySelectorAll('._1nuage2mots').forEach(ssgWordCloud);
